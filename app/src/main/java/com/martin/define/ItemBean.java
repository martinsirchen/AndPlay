package com.martin.define;

public class ItemBean {

    // 图片ID
    private int imageId;
    // 标题
    private String title;
    // 内容
    private String content;

    public ItemBean(int imageId, String title, String content) {
        this.imageId  = imageId;
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return this.title;
    }

    public int getImageId(){
        return this.imageId;
    }

    public String getContent(){
        return this.content;
    }
}
