package com.martin.define;

import java.util.ArrayList;
import java.util.List;

public class ItemGroup {
    private String title;
    private List<ItemBean> list;

    public ItemGroup(String title){
        this.title = title;
        list = new ArrayList<>();
    }

    public void addItem(ItemBean item) {
        list.add(item);
    }

    public Object getItem(int postion){
        if (postion == 0){
            return title;
        }else{
            return list.get(postion - 1);
        }
    }

    // title (1) + list (count)
    public int size(){
        return list.size() + 1;
    }

}
