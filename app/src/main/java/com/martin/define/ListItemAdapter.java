package com.martin.define;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.martin.andplay.R;

import java.util.List;

public class ListItemAdapter extends BaseAdapter {

    class ViewHolder{
        public ImageView ivImage;
        public TextView tvTitle;
        public TextView tvContent;
    }
    private List<ItemBean> itemList;
    private LayoutInflater inflater;

    public ListItemAdapter(Context context, List<ItemBean> list){
        itemList = list;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_item, null);

            viewHolder.ivImage = (ImageView) convertView.findViewById(R.id.lvImage);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.lvTitle);
            viewHolder.tvContent = (TextView) convertView.findViewById(R.id.lvContent);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ItemBean obj = (ItemBean)getItem(position);
        viewHolder.ivImage.setImageResource(obj.getImageId());
        viewHolder.tvTitle.setText(obj.getTitle());
        viewHolder.tvContent.setText(obj.getContent());
        return convertView;
    }
}
