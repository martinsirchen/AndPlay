package com.martin.define;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.martin.andplay.R;

import java.util.List;

public class GroupItemAdapter extends BaseAdapter{

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private List<ItemGroup> groups;
    private LayoutInflater inflater;

    public GroupItemAdapter(Context context, List<ItemGroup> groups){
        this.groups = groups;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        int count = 0;
        if (groups != null)
        {
            for (ItemGroup group : groups){
                count += group.size();
            }
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        int head = 0;
        for (ItemGroup group : groups){
            int size = group.size();
            int current = position - head;
            if (current < size)
                return group.getItem(current);
            head += size;
        }
        return null;
    }

    public int getGroupItemCount(int position){
        int head = 0;
        for (ItemGroup group : groups){
            if (position - head == 0)
                return group.size() - 1;
            head += group.size();
        }
        return 0;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        int head = 0;
        for (ItemGroup group : groups){
            if (position - head == 0)
                return TYPE_HEADER;
            head += group.size();
        }
        return TYPE_ITEM;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) == TYPE_ITEM;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    final class ViewHolder {
        ImageView imageView;
        TextView tvTitle, tvContent;
        TextView tvGroupHeader, tvGroupCount;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        switch (getItemViewType(position)){
            case TYPE_HEADER:
                if (convertView == null){
                    viewHolder = new ViewHolder();
                    convertView = inflater.inflate(R.layout.list_header, parent, false);
                    viewHolder.tvGroupHeader = (TextView)convertView.findViewById(R.id.tvGroupHeader);
                    viewHolder.tvGroupCount = (TextView)convertView.findViewById(R.id.tvGroupCount);
                    convertView.setTag(viewHolder);
                }else
                    viewHolder = (ViewHolder) convertView.getTag();
                viewHolder.tvGroupHeader.setText((CharSequence) getItem(position));
                viewHolder.tvGroupCount.setText(String.valueOf(getGroupItemCount(position)));
                break;
            case TYPE_ITEM:
                if (convertView == null){
                    viewHolder = new ViewHolder();
                    convertView = inflater.inflate(R.layout.list_item, parent, false);
                    viewHolder.imageView = (ImageView)convertView.findViewById(R.id.lvImage);
                    viewHolder.tvContent = (TextView)convertView.findViewById(R.id.lvContent);
                    viewHolder.tvTitle = (TextView)convertView.findViewById(R.id.lvTitle) ;
                    convertView.setTag(viewHolder);
                }else
                    viewHolder = (ViewHolder) convertView.getTag();
                ItemBean item = (ItemBean) getItem(position);
                viewHolder.imageView.setImageResource(item.getImageId());
                viewHolder.tvTitle.setText(item.getTitle());
                viewHolder.tvContent.setText(item.getContent());
                break;
        }
        return convertView;
    }
}
