package com.martin.andplay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.martin.define.MtIntentService;

public class IntentServiceActivity extends AppCompatActivity {

    private TextView tvServiceStatus, tvThreadStatus;
    private ProgressBar progressBar;
    private Button btnStartIntentService, btnStopIntentService;

    private MtBroadcastReceiver mtBroadcastReceiver;
    private LocalBroadcastManager localBroadcastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_service);
        tvServiceStatus = (TextView)findViewById(R.id.tvServiceStatus);
        tvThreadStatus = (TextView)findViewById(R.id.tvThreadStatus);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        btnStartIntentService = (Button)findViewById(R.id.btnStartIntentService);
        btnStartIntentService.setOnClickListener(onButtonClick);
        btnStopIntentService = (Button)findViewById(R.id.btnStopIntentService);
        btnStopIntentService.setOnClickListener(onButtonClick);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        mtBroadcastReceiver = new MtBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MtIntentService.ACTION_TYPE_SERVICE);
        intentFilter.addAction(MtIntentService.ACTION_TYPE_THREAD);
        localBroadcastManager.registerReceiver(mtBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(mtBroadcastReceiver);
    }

    private View.OnClickListener onButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnStartIntentService:
                    Intent startIntent = new Intent(IntentServiceActivity.this, MtIntentService.class);
                    startService(startIntent);
                    break;
                case R.id.btnStopIntentService:
                    Intent stopIntent = new Intent(IntentServiceActivity.this, MtIntentService.class);
                    stopService(stopIntent);
                    break;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        tvServiceStatus.setText("服务状态：未运行");
        tvThreadStatus.setText("线程状态：未运行");
        progressBar.setProgress(0);
    }

    public class MtBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            switch ((intent.getAction())){
                case MtIntentService.ACTION_TYPE_SERVICE:
                    tvServiceStatus.setText("服务状态：" + intent.getStringExtra(MtIntentService.KEY_STATUS));
                    break;
                case MtIntentService.ACTION_TYPE_THREAD:
                    tvThreadStatus.setText("线程状态：" + intent.getStringExtra(MtIntentService.KEY_STATUS));
                    progressBar.setProgress(intent.getIntExtra(MtIntentService.KEY_PROGRESS, 0));
                    break;
            }
        }
    }
}
