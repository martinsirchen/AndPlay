package com.martin.andplay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        findViewById(R.id.btnSharpTest).setOnClickListener(this);
        findViewById(R.id.btnListViewTest).setOnClickListener(this);
        findViewById(R.id.btnIntentService).setOnClickListener(this);
    }

    public static final String LISTVIEW_SHOW_TYPE = "ShowType";
    public static final int LISTVIEW_NORMAL = 0;  //简单列表
    public static final int LISTVIEW_CUSTOM = 1;  //自定义列表
    public static final int LISTVIEW_CUSTOM_GROUP = 2; // 自定义列表分组

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnSharpTest:
                startActivity(new Intent(MainActivity.this, SharpActivity.class));
                break;
            case R.id.btnListViewTest:
                Intent intent = new Intent(MainActivity.this, ListViewActivity.class);
                intent.putExtra(LISTVIEW_SHOW_TYPE, LISTVIEW_CUSTOM_GROUP);
                startActivity(intent);
                break;
            case R.id.btnIntentService:
                startActivity(new Intent(MainActivity.this, IntentServiceActivity.class));
                break;
        }
    }
}
