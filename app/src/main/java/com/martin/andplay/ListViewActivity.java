package com.martin.andplay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.martin.define.GroupItemAdapter;
import com.martin.define.ListItemAdapter;
import com.martin.define.ItemBean;
import com.martin.define.ItemGroup;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {

    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        Intent intent = getIntent();
        int viewType = intent.getIntExtra(MainActivity.LISTVIEW_SHOW_TYPE, MainActivity.LISTVIEW_NORMAL);
        lv = (ListView) findViewById(R.id.lvTest);
        initView(viewType);
    }

    private void initView(int type) {
        switch (type) {
            case MainActivity.LISTVIEW_NORMAL:
                LoadNormalList();
                break;
            case MainActivity.LISTVIEW_CUSTOM:
                LoadCustomList();
                break;
            case MainActivity.LISTVIEW_CUSTOM_GROUP:
                LoadCustomGroupList();
                break;
        }
    }

    private void LoadCustomGroupList() {
        ItemGroup group;
        ItemBean item;
        List<ItemGroup> list = new ArrayList<ItemGroup>();
        group = new ItemGroup("Group 1");
        item = new ItemBean(R.raw.red, "测试1", "测试1测试1测试1");
        group.addItem(item);
        item = new ItemBean(R.raw.yellow, "测试2", "测试2测试2测试2");
        group.addItem(item);
        item = new ItemBean(R.raw.magenta, "测试3", "测试3测试3测试3");
        group.addItem(item);
        list.add(group);
        group = new ItemGroup("Group 2");
        item = new ItemBean(R.raw.orange, "测试4", "测试4测试4测试4");
        group.addItem(item);
        item = new ItemBean(R.raw.rose, "测试5", "测试5测试5测试5");
        group.addItem(item);
        list.add(group);
        group = new ItemGroup("Group 3");
        item = new ItemBean(R.raw.yellow, "测试6", "测试6测试6测试6");
        group.addItem(item);
        list.add(group);
        GroupItemAdapter adapter = new GroupItemAdapter(this, list);
        lv.setAdapter(adapter);
    }

    private void LoadNormalList() {
        ArrayList<String> dataList = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            dataList.add("第" + (i + 1) + "条记录");
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ListViewActivity.this,
                android.R.layout.simple_list_item_2, dataList);
        lv.setAdapter(adapter);
    }

    private void LoadCustomList() {
        List<ItemBean> list = new ArrayList<ItemBean>();
        ItemBean item = new ItemBean(R.raw.red, "测试1", "测试1测试1测试1");
        list.add(item);
        item = new ItemBean(R.raw.yellow, "测试2", "测试2测试2测试2");
        list.add(item);
        item = new ItemBean(R.raw.magenta, "测试3", "测试3测试3测试3");
        list.add(item);
        item = new ItemBean(R.raw.orange, "测试4", "测试4测试4测试4");
        list.add(item);
        item = new ItemBean(R.raw.rose, "测试5", "测试5测试5测试5");
        list.add(item);
        ListItemAdapter adapter = new ListItemAdapter(this, list);
        lv.setAdapter(adapter);
    }
}
